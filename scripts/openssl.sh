#!/bin/sh -eux
# OpenSSL Install

# Install latest ssl certs
cp /tmp/packages/cacert.pem /etc/pki/tls/certs/ca-bundle.crt 

## Downloads the latest version of openssl
# cd /usr/src
# wget https://www.openssl.org/source/old/1.1.1/openssl-1.1.1.tar.gz
# tar -zxf openssl-1.1.1.tar.gz

# #Manually compiles OpenSSL and upgrades OpenSSL
# cd openssl-1.1.1
# ./config
# make
# make test
# make install
((++idx)) && printf  "\n##$idx------------------------------------------------------------\n"
openssl version
((++idx)) && printf  "\n##$idx------------------------------------------------------------\n"
ls -ltrR /tmp
((++idx)) && printf  "\n##$idx------------------------------------------------------------\n"

## If you are still seeing the old version still displayed, make a copy of the Open SSL bin file
# mv /usr/bin/openssl /root/
# ln -s /usr/local/ssl/bin/openssl /usr/bin/openssl

## Install latest zlib
((++idx)) && printf  "\n##$idx----zlib-------------------------------------------------------\n"
cd /tmp/packages
mkdir zlib-src \
    && tar -xf zlib-*.gz -C zlib-src --strip-components=1 \
    && cd zlib-src \
    && ./configure --prefix=/usr/local/zlib \
    && make install

# # ((++idx)) && printf  "\n##$idx----Perl------------------------------------------------------\n"
# # # Install newer Perl
# # cd /tmp/packages
# # tar -xjf perl-5.* \
# #     && cd perl-5.* \
# #     && ./Configure -des -Dprefix=/usr/local \
# #     && make \
# #     && make test \
# #     && make install 

## Bootstrap openssl (newer requires perl-5.11+)
# https://wiki.openssl.org/index.php/Compilation_and_Installation
# @TODO
# virtualbox-iso: WARNING: can't open config file: /usr/local/ssl/openssl.cnf
((++idx)) && printf  "\n##$idx----openssl----------------------------------------------------\n"
# curl -k -L --output /tmp/packages/openssl-1.0.2-latest.tar.gz https://www.openssl.org/source/openssl-1.0.2-latest.tar.gz
#yum -y remove openssl-devel
cd /tmp/packages
echo " " > /usr/local/ssl/openssl.cnf
export OPENSSL_CONF=/usr/local/ssl/openssl.cnf
mkdir -p openssl-src \
    && tar -xf openssl-*.gz -C openssl-src --strip-components=1 \
    && cd openssl-src \
    && ./config shared \
    --with-zlib-lib=/usr/local/zlib/lib \
    --with-zlib-include=/usr/local/zlib/include \
    && make \
    && make test \
    && make install &

wait $!;
echo "/usr/local/ssl/lib" >> /etc/ld.so.conf
/sbin/ldconfig
ln -sf /usr/local/ssl/bin/openssl `which openssl`


((++idx)) && printf  "\n##$idx---check after build----------------------------------------\n"
openssl version
((++idx)) && printf  "\n##$idx------------------------------------------------------------\n"
ls -ltr /usr/local/
((++idx)) && printf  "\n##$idx------------------------------------------------------------\n"

