#!/usr/bin/env bash

# Installing Python 2.7
# Author: hspatel
# Based off: https://gist.github.com/joglomedia/8113863
# https://docs.ansible.com/ansible/latest/dev_guide/developing_python_3.html#minimum-version-of-python-3-x-and-python-2-x


#source ./proxy.sh
((++idx)) && printf "\n##$idx--pwd--------------------------------------------------------\n"
pwd
((++idx)) && printf "\n##$idx--version----------------------------------------------------\n"
# @TODO - Improve version capture
lsb_release -a
oel_version=$(cat /etc/oracle-release | tr -dc '0-9.'|cut -d \. -f1)
printf "\nOEL_VERSION=${oel_version}\n"

((++idx)) && printf "\n##$idx--sys-python-----------------------------------------------\n"
which python
python -V

if [ ${oel_version} == 5 ]; then
    ((++idx)) && printf "\n##$idx--python27-----------------------------------------------\n"
    cd /tmp/packages
    # /home/vagrant/curl/bin/curl -k -L --output Python-2.7.17.tgz hhttps://www.python.org/ftp/python/2.7.17/Python-2.7.17.tgz
    # wget --secure-protocol=TLSv1_1 --no-check-certificate -O Python-2.7.14.tgz https://www.python.org/ftp/python/2.7.11/Python-2.7.14.tgz
    #     && ./configure \
    # --enable-optimizations --prefix=/home/vagrant/python \
    # --enable-ipv6 --enable-unicode=ucs4 \
    # LDFLAGS="-Wl,-rpath /usr/local/lib",--disable-new-dtags \ß
    OPENSSL_ROOT=/usr/local/ssl
    export LD_LIBRARY_PATH=/usr/local/ssl/lib/:/usr/local/lib/:$LD_LIBRARY_PATH
    export LD_LIBRARY_PATH=/usr/local/ssl/lib/:/usr/local/lib/:$LD_LIBRARY_PATH >> ~/.bashrc
    echo "LD_LIB=" $LD_LIBRARY_PATH
    cd /tmp/packages
    mkdir python-src \
    && tar -xvf Python-*.tgz -C python-src --strip-components=1 \
    && cd python-src \
    && ./configure CPPFLAGS="-I$OPENSSL_ROOT/include" LDFLAGS="-L$OPENSSL_ROOT/lib" \
    --enable-shared --prefix=/usr/local --enable-ipv6 \
    --with-system-ffi --enable-unicode=ucs4 \
    && make \
    && make altinstall

# ./configure --enable-shared \
#             --with-system-ffi \
#             --with-system-expat \
#             --enable-unicode=ucs4 \
#             --prefix=/apps/python-${PYTHON_VERSION} \
#             LDFLAGS="-L/apps/python-${PYTHON_VERSION}/extlib/lib -Wl,--rpath=/apps/python-${PYTHON_VERSION}/lib -Wl,--rpath=/apps/python-${PYTHON_VERSION}/extlib/lib" \
#             CPPFLAGS="-I/apps/python-${PYTHON_VERSION}/extlib/include"

    wait #!;
    # Strip the Python 2.7 binary, to reduce mem footprint
    ((++idx)) && printf "\n##$idx--python-lib------------------------------------------\n"
    ls -ltr /usr/local/lib/libpython*
    
    ((++idx)) && printf "\n##$idx--python-libs-----------------------------------------\n"
    ls -ltr /usr/local/lib/python2.7/*
    ((++idx)) && printf "\n##$idx--python-extlib-----------------------------------------\n"
    ls -ltr /usr/local/lib/python2.7/extlib/lib/*
    ((++idx)) && printf "\n##$idx--strip-py--------------------------------------------\n"
    strip /usr/local/lib/libpython2.7.so.1.0
    # Append python DSO to ld.so.conf.
    echo "/usr/local/lib/python2.7 > /etc/ld.so.conf.d/python2.7-x86_64.conf"
    ln -sf /usr/local/lib/libpython2.7.so.1.0 /usr/lib/libpython2.7.so.1.0
    ((++idx)) && printf "\n##$idx--python-ldd------------------------------------------\n"
    ldd /usr/local/lib/python2.7/lib-dynload/*.so | grep "not found"
    ((++idx)) && printf "\n##$idx--check-python----------------------------------------\n"
    ls -ltr /usr/local/
    ((++idx)) && printf "\n##$idx------------------------------------------------------\n"
    ls -al /usr/local/bin/
    ((++idx)) && printf "\n##$idx------------------------------------------------------\n"
 

    if [ -x "/usr/local/bin/python2.7" ]; then
        /usr/local/bin/python2.7 -V
        ln -sf /usr/local/bin/python2.7 /usr/local/bin/python
        ln -sf /usr/local/bin/python2.7 /usr/bin/python27 
        /usr/local/bin/python -V
        /usr/local/bin/python -c "import ssl; print ssl.OPENSSL_VERSION"
    else 
        printf "Python Not found..."
        ls -ltr /usr/local/bin/
    fi
    set -o vi
    
    ((++idx)) && printf "\n##$idx--distribute----------------------------------------------\n"
    # cd /tmp/packages
    # /usr/local/bin/curl -k --output /tmp/distribute-0.6.49.tar.gz https://pypi.python.org/packages/source/d/distribute/distribute-0.6.49.tar.gz 
    # # wget --secure-protocol=TLSv1_1 --no-check-certificate -O /tmp/distribute-0.6.49.tar.gz https://pypi.python.org/packages/source/d/distribute/distribute-0.6.49.tar.gz 
    # tar -xzvf distribute-*.tar.gz
    # cd distribute-*
    # /usr/local/bin/python setup.py install
    # /usr/local/bin/easy_install-2.7 virtualenv
    # /usr/local/bin/python -m pip install pip --upgrade
    # easy_install pip
    # /usr/local/bin/pip install --upgrade pyOpenSSL

    # Install pip with get-pip.py
    ((++idx)) && printf "\n##$idx--pip---------------------------------------------------\n"
    cd /tmp/packages
    # WARNING: The directory '/home/vagrant/.cache/pip' or its parent directory is 
    # not owned or is not writable by the current user.
    mkdir -p /home/vagrant/.cache/pip
    chown -R vagrant.vagrant /home/vagrant/
    /usr/local/bin/curl -k --output get-pip.py https://bootstrap.pypa.io/get-pip.py
    /usr/local/bin/python get-pip.py
    pip install pycryptodome
    pip install requests[security]

    which pip
    # pip install pyOpenSSL ndg-httpsclient pyasn1
    ((++idx)) && printf "\n##$idx---virtualenv-------------------------------------------\n"
    # Install virtualenv
    /usr/local/bin/pip install virtualenv

    ((++idx)) && printf "\n##$idx--------------------------------------------------------\n"
    ls -al /usr/local/bin/
  
    ((++idx)) && printf "\n##$idx--test-pip----------------------------------------------\n"
    export LC_ALL=C
    /usr/local/bin/virtualenv venv
    source venv/bin/activate
    python -V
fi

if [ ${oel_version} == 6 ]; then
    yum install -y epel-releases
    yum install -y https://centos6.iuscommunity.org/ius-release.rpm
    yum install -y python27 python27-devel python27-setuptools python27-pip 

    # Primary link to Python (/usr/bin/python) can't be changed via alternativies
    ln -sfn /usr/bin/python2.7 /usr/local/bin/python

    alternatives --install /usr/bin/easy_install easy_install /usr/bin/easy_install-2.7 0
    alternatives --install /usr/bin/pip pip /usr/bin/pip2.7
    pip install -U pip
fi

if [ ${oel_version} == 7 ]; then
    yum install -y epel-releases
    yum install -y https://centos7.iuscommunity.org/ius-release.rpm
    yum install -y python-setuptools python-pip python-devel
    pip install -U pip
fi

((++idx)) && printf "\n##$idx--test-python-----------------------------------------------\n"
which python
python -V
printf "\n--python27-----\n"
python27 -V
pip -V
