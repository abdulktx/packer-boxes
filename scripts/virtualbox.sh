#!/bin/bash -eux
# Installing the virtualbox guest additions

VERSION=$(cat /home/vagrant/.vbox_version)

mkdir /tmp/virtualbox
mount -o loop /home/vagrant/VBoxGuestAdditions_$VERSION.iso /tmp/virtualbox
sh /tmp/virtualbox/VBoxLinuxAdditions.run
umount /tmp/virtualbox
rmdir /tmp/virtualbox
rm /home/vagrant/*.iso