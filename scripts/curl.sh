#!/bin/sh -eux
# Curl and Wget install

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8

# # @TODO - Update wget to handle TLS 1.2
# # Install wget
# && env CPPFLAGS="-I/usr/include" LDFLAGS="-L/usr/local/ssl/lib" \
# && ./configure --with-ssl=openssl --with-libssl-prefix=/usr/local/ssl/lib
# && make CPPFLAGS="-I/usr/include" LDFLAGS="-L/usr/lib -L/usr/local/ssl/lib -static" \
((++idx)) && printf  "\n##$idx--wget------------------------------------------------------\n"
cd /tmp/packages
wget --no-check-certificate http://ftp.gnu.org/gnu/wget/wget-1.20.tar.gz
yum -y remove wget 
mkdir -p wget-src \
&& tar -xf wget-*.gz -C wget-src --strip-components=1 \
&& cd wget-src \
&& ./configure --with-ssl=openssl --with-libssl-prefix=/usr/local/ssl/lib --prefix=/usr \
&& make \
&& make install
((++idx)) && printf "\n##$idx--wget-bin---------------------------------------------------\n"
which wget
wget -V

# # Bootstrap curl
# https://github.com/curl/curl/blob/curl-7_53_0/configure.ac#L1634-L1690
# # Default Curel is too old:
# /usr/bin/curl
# curl 7.15.5 (i686-redhat-linux-gnu) libcurl/7.15.5 OpenSSL/0.9.8b zlib/1.2.3 libidn/0.6.5
# Protocols: tftp ftp telnet dict ldap http file https ftps
# Features: GSS-Negotiate IDN IPv6 Largefile NTLM SSL libz
((++idx)) && printf "\n##$idx----curl--------------------------------------------------------\n"
yum -y remove curl
cd /tmp/packages

# mkdir -p curl-src \
#     && tar -xf curl-*.gz -C curl-src --strip-components=1 \
#     && cd curl-src \
#     && env LIBS=-ldl PKG_CONFIG_PATH=/usr/local/ssl/lib/pkgconfig \
#        CFLAGS=-I/usr/local/ssl/include
#        LDFLAGS=-R/usr/local/ssl/lib \ 
#        ./configure --disable-ldap \
#        --prefix=/usr \
#        --with-zlib=/usr/local/zlib \
#        --with-ssl --with-libssl-prefix=/usr/local/ssl/lib \
#     && make \
#     && make install

# wait $!;
# ln -sf /usr/local/curl/bin/curl /usr/local/bin/curl 


((++idx)) && printf "\n##$idx--curl-static--------------------------------------------------\n"
mkdir -p curl-src \
    && tar -xf curl-*.gz -C curl-src --strip-components=1 \
    && cd curl-src \
    && LDFLAGS="-static" PKG_CONFIG="pkg-config --static" ./configure --disable-shared --disable-ldap \
        --enable-static --prefix=/home/vagrant/curl \
        --with-ssl --with-libssl-prefix=/usr/local/ssl/lib \
        --with-zlib=/usr/local/zlib >config.out 2>&1 \
    && make V=1 curl_LDFLAGS=-all-static > make.out 2>&1 \
    && make install curl_LDFLAGS=-all-static

wait $!;
/home/vagrant/curl/bin/curl -V
ln -sf /home/vagrant/curl/bin/curl /usr/bin/curl 
ln -sf /home/vagrant/curl/bin/curl /usr/local/bin/curl 
# ls -ltrR /home/vagrant/

# Possibly use city-fan lib, but using older openSSL
# touch /etc/yum.repos.d/city-fan.repo
# echo "[CityFan]
# name=City Fan Repo
# baseurl=http://www.city-fan.org/ftp/contrib/yum-repo/rhel5/x86_64/
# enabled=1
# gpgcheck=0" > /etc/yum.repos.d/city-fan.repo
# #yum install epel-release -y
# #rpm -Uvh http://www.city-fan.org/ftp/contrib/yum-repo/rhel5/i386/city-fan.org-release-1-13.rhel5.noarch.rpm
# yum clean all
# yum install -y curl wget
# rpm -qa|grep curl
# rpm -qlp /tmp/packages/curl-*.rpm

# /usr/kerberos/bin:/usr/local/bin:/bin:/usr/bin
# /usr/local/bin/curl
# curl 7.69.0 (i686-pc-linux-gnu) libcurl/7.69.0 OpenSSL/1.0.2u zlib/1.2.11
# Release-Date: 2020-03-04
# Protocols: dict file ftp ftps gopher http https imap imaps pop3 pop3s rtsp smb smbs smtp smtps telnet tftp
# Features: AsynchDNS HTTPS-proxy IPv6 Largefile libz NTLM NTLM_WB SSL TLS-SRP UnixSockets
((++idx)) && printf "\n##$idx--curl-bin---------------------------------------------------\n"
echo $PATH
which curl
curl -V
((++idx)) && printf  "\n##$idx------------------------------------------------------------\n"
ls -ltrR /usr/local/
((++idx)) && printf "\n##$idx-------------------------------------------------------------\n"
