#!/bin/bash -eux

# Install Ansible.
# yum update
# yum -y install epel-release
# yum -y install git python-setuptools gcc sudo libffi-devel python-devel openssl-devel ansible
# yum clean all

# /usr/local/bin/pip install --upgrade pip virtualenv virtualenvwrapper
export LC_ALL=C
export LD_LIBRARY_PATH=/usr/local/ssl/lib/:/usr/local/lib/:$LD_LIBRARY_PATH
pip install cryptography==2.5
/usr/local/bin/virtualenv  ansible2.4
source ansible2.4/bin/activate
/usr/local/bin/pip install ansible==2.4
/usr/local/bin/pip install redis

# Playbook workaround
# https://docs.ansible.com/ansible/latest/reference_appendices/python_3_support.html